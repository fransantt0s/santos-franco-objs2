public class PuntoDeVenta {
    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String cargarCreditoYRegistrarNumero(double saldo){
        this.getUsuario().getCelular().cargarSaldo(saldo);
        return this.getUsuario().getCelular().getNumero();
    }

    public String patenteDelusuario(){
        return this.getUsuario().getPatente();
    }

    public String nroCelularUsuario(){
        return this.getUsuario().getCelular().getNumero();
    }


}
