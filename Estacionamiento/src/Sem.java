import java.util.*;

public class Sem  {
    private final double precioPorHora=40;
    private ArrayList <ZonaDeEstacionamiento> zonas;
    private final int horaInicio=7;
    private final int horaFinalizacion =20;

    public double getPrecioPorHora() {
        return precioPorHora;
    }

    public ArrayList<ZonaDeEstacionamiento> getZonas() {
        return zonas;
    }

    public ArrayList<PuntoDeVenta> getPuntosDeVentas(){
        ArrayList<PuntoDeVenta> puntos = new ArrayList<>();
        for (int i=0;i<zonas.size(); i++){
            puntos.add(zonas.get(i).getPuntosDeVenta().get(i));
        }
        return puntos;
    }

    public void setZonas(ArrayList<ZonaDeEstacionamiento> zonas) {
        this.zonas = zonas;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public int getHoraFinalizacion() {
        return horaFinalizacion;
    }

    @Override
    public void update(Observable o, Object arg) {


    }
}
