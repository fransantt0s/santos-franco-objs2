public class Usuario {
    private String patente;
    private Celular celular;

    public Usuario(String patente,Celular celular){
        this.patente = patente;
        this.celular = celular;

    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }


}
