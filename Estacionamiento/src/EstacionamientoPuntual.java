import java.util.Observable;

public class EstacionamientoPuntual extends Observable{
    private int horasEstacionamiento;
    private String patenteUsuario;
    private Sem sistema;

    public int getHorasEstacionamiento() {
        return horasEstacionamiento;
    }

    public void setHorasEstacionamiento(int horasEstacionamiento) {
        this.horasEstacionamiento = horasEstacionamiento;
        setChanged();
        notifyObservers(horasEstacionamiento);

    }


    public void notificar(String mensaje){

    }

    public String getPatenteUsuario() {
        return patenteUsuario;
    }

    public void setPatenteUsuario(String patenteUsuario) {
        this.patenteUsuario = patenteUsuario;
        setChanged();
        notifyObservers(patenteUsuario);
    }

    public void cobrar(String patente, int horas){
        this.setHorasEstacionamiento(horas);
        this.setPatenteUsuario(patente);

    }


}
