package trabajador;

import ingreso.Ingreso;

import java.util.ArrayList;
import java.util.List;

public class Trabajador {
    private String nombre;
    private Ingreso ingresoAnio;

    public Trabajador(String nombre, Ingreso ingresoAnio) {
        this.nombre = nombre;
        this.ingresoAnio = ingresoAnio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Ingreso getIngresoAnio() {
        return ingresoAnio;
    }

    public void setIngresoAnio(Ingreso ingresoAnio) {
        this.ingresoAnio = ingresoAnio;
    }

    public double getMontoImponible(){
        return this.ingresoAnio.montoImponible();
    }

    public double getImpuestoAPagar(){
        return (this.ingresoAnio.montoImponible() *2) / 100;
    }


}
