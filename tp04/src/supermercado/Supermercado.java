package supermercado;

import producto.Producto;

import java.util.ArrayList;
import java.util.List;

public class Supermercado {
    private String nombre;
    private String direccion;
    List <Producto> productos = new ArrayList<Producto>();

    public Supermercado(String nombre, String direccion, List<Producto> productos) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.productos = productos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public int cantProductos(){
        return this.getProductos().size();
    }

    public double precioTotalDeProductos(){
        double precio = 0 ;
        for (int i=0;i<this.getProductos().size();i++){
            precio += this.getProductos().get(i).getPrecio();
        }
        return precio;
    }




}
