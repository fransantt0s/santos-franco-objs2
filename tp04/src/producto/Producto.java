package producto;

public class Producto {
    private String nombre;
    private double precio;
    private boolean esPrimeraNecesidad;
    private int descuento;

    public Producto(String nombre, double precio, boolean esPrimeraNecesidad,int descuento) {
        this.nombre = nombre;
        this.precio = precio;
        this.esPrimeraNecesidad = esPrimeraNecesidad;
        this.descuento= descuento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        if( this.esPrimeraNecesidad){
            return this.precio  - (this.precio * descuento) / 100;
        }
        else{
            return this.getPrecio();
        }
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isEsPrimeraNecesidad() {
        return esPrimeraNecesidad;
    }

    public void setEsPrimeraNecesidad(boolean esPrimeraNecesidad) {
        this.esPrimeraNecesidad = esPrimeraNecesidad;
    }
}
