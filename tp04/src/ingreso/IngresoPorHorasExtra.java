package ingreso;

public class IngresoPorHorasExtra extends Ingreso {
    private int cantHorasExtras;

    public IngresoPorHorasExtra(String mes, String concepto, double monto,int cantHorasExtras) {
        super(mes, concepto, monto);
        this.cantHorasExtras=cantHorasExtras;
    }

    public double montoImponible(){
        return 0;
    }
}
