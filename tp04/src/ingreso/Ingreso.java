package ingreso;

public class Ingreso {
    private String mes;
    private String concepto;
    private double monto;

    public Ingreso(String mes, String concepto, double monto) {
        this.mes = mes;
        this.concepto = concepto;
        this.monto = monto;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double montoImponible(){
        return this.getMonto();
    }

}
